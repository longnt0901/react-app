import { useDetectDevice } from './hooks/detect-device';
import DesktopRouters from './routers/router.desktop';
import MobileRouters from './routers/router.mobile';

function App() {
  const isMobile: boolean = useDetectDevice();

  return (
    <div className={isMobile ? 'app mobile' : 'app'}>
      {isMobile ? <MobileRouters /> : <DesktopRouters />}
    </div>
  );
}

export default App;
