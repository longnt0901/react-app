import { useState } from 'react';

interface IProps {
  width: number;
  height: number;
}

const FabricJS = (props: IProps): JSX.Element => {
  const { width, height } = props;
  const [state, setState] = useState(null);
  return <canvas id="editor" width={width} height={height}></canvas>;
};

export default FabricJS;
