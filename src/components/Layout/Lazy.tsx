import { useState } from "react";

const Lazy = (props: any) => {
  const [isLoading, setLoading] = useState(true);
  return (
    <div className="page-loading">{isLoading ? 'Loading...' : props.component}</div>
  );
};

export default Lazy;
