import { useLayoutEffect, useState } from 'react';
import { MAX_MOBILE_SIZE } from '../constants/app';
import { isMobile as isMobileDevice } from '../libs/Utils/Browser';

export const useDetectDevice = (width?: number) => {
  const [isMobile, setIsMobile] = useState(false);
  const size = width ? width : MAX_MOBILE_SIZE;

  useLayoutEffect(() => {
    function detectDevice() {
      if (isMobileDevice()) {
        setIsMobile(true);
        return;
      }

      setIsMobile(window.innerWidth < size);
    }

    if (!isMobileDevice()) {
      window.addEventListener('resize', detectDevice);
    }
    detectDevice();
    return () => window.removeEventListener('resize', detectDevice);
  });

  return isMobile;
};
