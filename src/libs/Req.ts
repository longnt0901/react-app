import axios, { AxiosResponse } from 'axios';

declare module 'axios' {
  export interface AxiosRequestConfig {
    byPassSign?: boolean;
  }
}

const instance = axios.create({
  headers: {},
});

const handleResponse = (res: AxiosResponse) => {
  switch (res.status) {
    case 401:
      break;
  }
};

instance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (!navigator.onLine) {
      return Promise.reject();
    }

    if (error && error.response) {
      handleResponse(error.response);
    }

    return Promise.reject();
  },
);

export default instance;
