const browserInfo = () => {};
const isMobile = (): boolean => {
  return /iPhone|iPod|Android/i.test(navigator.userAgent);
};
const isPC = () => {
  return !isMobile();
};

export { isMobile, isPC, browserInfo };
