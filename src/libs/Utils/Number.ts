const px2Mm = (num: number): number => {
  return num * 0.2645833333;
};

const mm2Px = (num: number): number => {
  return num * 3.7795275591;
};

const pt2Px = (num: number): number => {
  return num * 1.3333333333333333;
};

const px2Pt = (num: number): number => {
  return num * 0.75292857248934;
};

const mm2Inch = (num: number): number => {
  return num * 0.0393700787;
};

const inch2Mm = (num: number): number => {
  return num * 25.4;
};

const round = (): number => {
  return 0;
};

export { px2Mm, mm2Px, pt2Px, px2Pt, mm2Inch, inch2Mm, round };
