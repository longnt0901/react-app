import { useEffect } from 'react';

const About = (): JSX.Element => {
  useEffect(() => {
    document.title = 'About';
  }, []);

  return <>About</>;
};

export default About;
