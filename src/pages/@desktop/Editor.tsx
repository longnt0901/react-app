import { lazy, useEffect } from 'react';

const MainEditor = lazy(() => import('./Editor/MainEditor'));
const Navigation = lazy(() => import('./Editor/Navigation'));
const RightSidebar = lazy(() => import('./Editor/RightSidebar'));
const Sidebar = lazy(() => import('./Editor/Sidebar'));

const Editor = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Editor';
  }, []);

  return (
    <>
      <div className="container">
        <div className="sidebar">
          <Sidebar />
        </div>
        <div className="content">
          <div className="navigation">
            <Navigation />
          </div>
          <div className="main-editor">
            <MainEditor />
          </div>
          <div className="right-sidebar">
            <RightSidebar />
          </div>
        </div>
      </div>
    </>
  );
};

export default Editor;
