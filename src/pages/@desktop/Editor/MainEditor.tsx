import ShortcutKeys from "../../../libs/ShortcutKeys";

const MainEditor = (): JSX.Element => {

  // init events
  new ShortcutKeys(document.getElementById('svg'));

  return <>Main Editor</>;
};

export default MainEditor;
