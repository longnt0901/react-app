import { useEffect } from 'react';

const Help = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Help';
  }, []);

  return <>Help</>;
};

export default Help;
