import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/imgs/logo.svg';
import '../../assets/styles/@desktop/home.scss';
import { toUrl } from '../../libs/Utils/Url';

const Home = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Home';
  }, []);

  return (
    <div className="home container">
      <header className="app-header">
        <img src={logo} className="app-logo" alt="logo" />
        <p>
          Edit <code>src/pages/@desktop/Home.tsx</code> and save to reload.
        </p>
        <a
          className="app-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <ol>
          <li>
            <Link className="app-link" to={toUrl(`/help`)}>
              Help
            </Link>
          </li>
          <li>
            <Link className="app-link" to={toUrl(`/editor/edit_id`)}>
              Editor
            </Link>
          </li>
          <li>
            <Link className="app-link" to={toUrl(`/term`)}>
              Terms
            </Link>
          </li>
          <li>
            <Link className="app-link" to={toUrl(`/about`)}>
              About
            </Link>
          </li>
        </ol>
      </header>
    </div>
  );
};

export default Home;
