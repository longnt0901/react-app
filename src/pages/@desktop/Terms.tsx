import { useEffect } from 'react';

const Terms = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Terms';
  }, []);

  return <>Terms</>;
};

export default Terms;
