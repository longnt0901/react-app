import { useEffect } from 'react';
import QRCode from 'qrcode.react';

const About = (): JSX.Element => {
  useEffect(() => {
    document.title = '[Mobile] About';
  }, []);

  return (
    <>
      <div>About Mobile</div>
      <QRCode value="http://facebook.github.io/react/" />
    </>
  );
};

export default About;
