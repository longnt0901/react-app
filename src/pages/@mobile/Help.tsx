import { useEffect } from 'react';

const Help = (): JSX.Element => {
  useEffect(() => {
    document.title = '[Mobile] Help';
  }, []);

  return <>Help Mobile</>;
};

export default Help;
