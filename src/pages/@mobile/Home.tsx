import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import '../../assets/styles/@mobile/home.scss';
import { toUrl } from '../../libs/Utils/Url';

const Home = (): JSX.Element => {
  useEffect(() => {
    document.title = '[Mobile] Home';
  }, []);

  return (
    <div className="container md">
      <ol>
        <li>
          <Link className="app-link" to={toUrl(`/help`)}>
            Help
          </Link>
        </li>
        <li>
          <Link className="app-link" to={toUrl(`/editor/edit_id`)}>
            Editor
          </Link>
        </li>
        <li>
          <Link className="app-link" to={toUrl(`/term`)}>
            Terms
          </Link>
        </li>
        <li>
          <Link className="app-link" to={toUrl(`/about`)}>
            About
          </Link>
        </li>
      </ol>
    </div>
  );
};

export default Home;
