import { useEffect } from 'react';

const Terms = (): JSX.Element => {
  useEffect(() => {
    document.title = '[Mobile] Terms';
  }, []);

  return <>Terms Mobile</>;
};

export default Terms;
