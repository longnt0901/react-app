import { combineReducers } from '@reduxjs/toolkit';
import test from './test';

const rootReducers = combineReducers({
  test
});

export default rootReducers;
