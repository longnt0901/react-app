import { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Loading from '../components/@desktop/Loading';

const Home = lazy(() => import('../pages/@desktop/Home'));
const About = lazy(() => import('../pages/@desktop/About'));
const Editor = lazy(() => import('../pages/@desktop/Editor'));
const Help = lazy(() => import('../pages/@desktop/Help'));
const Terms = lazy(() => import('../pages/@desktop/Terms'));

const Routers = (): JSX.Element => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route
          exact
          path={['/editor', '/editor/:poster_id']}
          component={Editor}
        />
        <Route exact path="/about" component={About} />
        <Route exact path="/help" component={Help} />
        <Route exact path="/term" component={Terms} />
        <Redirect to="/" />
      </Switch>
    </Suspense>
  );
};

export default Routers;
