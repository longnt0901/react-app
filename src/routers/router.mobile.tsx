import { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Loading from '../components/@mobile/Loading';
import '../assets/styles/@mobile/common.scss';

const Home = lazy(() => import('../pages/@mobile/Home'));
const About = lazy(() => import('../pages/@mobile/About'));
const Editor = lazy(() => import('../pages/@mobile/Editor'));
const Help = lazy(() => import('../pages/@mobile/Help'));
const Terms = lazy(() => import('../pages/@mobile/Terms'));

const Routers = (): JSX.Element => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route
          exact
          path={['/editor', '/editor/:poster_id']}
          component={Editor}
        />
        <Route exact path="/about" component={About} />
        <Route exact path="/help" component={Help} />
        <Route exact path="/term" component={Terms} />
        <Redirect to="/" />
      </Switch>
    </Suspense>
  );
};

export default Routers;
