// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import Enzyme, { EnzymeAdapter } from 'enzyme';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
});

class ImageData {
  readonly width: number;
  readonly height: number;
  readonly data: Uint8ClampedArray;

  constructor(sw: number, sh: number) {
    this.width = sw;
    this.height = sh;
    this.data = new Uint8ClampedArray([sw, sh]);
  }
}

window.ImageData = window.ImageData || ImageData;
